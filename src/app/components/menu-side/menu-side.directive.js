(function() {
	'use strict';

	angular
	.module('todo')
	.directive('menuside', function() {
		return {
			scope: true,
			restrict: 'AE',
			replace: 'true',
			templateUrl: 'app/components/menu-side/menu-side.html' 
		};
	})

})();